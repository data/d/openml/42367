# OpenML dataset: treasury

https://www.openml.org/d/42367

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: KEEL - [original](https://sci2s.ugr.es/keel/dataset.php?cod=42) - Date unknown  
**Please cite**:   

**Treasury Dataset**

This file contains the Economic data information of USA from 01/04/1980 to 02/04/2000 on a weekly basis. From given features, the goal is to predict 1 Month CD Rate.

**Attributes Information**
     1. 1Y-CMaturityRate real [77.055, 142.645]
     2. 30Y-CMortgageRate real [3.02, 17.15]
     3. 3M-Rate-AuctionAverage real [6.49, 18.63]
     4. 3M-Rate-SecondaryMarket real [2.67, 16.75]
     5. 3Y-CMaturityRate real [2.69, 16.76]
     6. 5Y-CMaturityRate real [4.09, 16.47]
     7. BankCredit real [4.17, 16.13]
     8. Currency real [1130.9, 4809.2]
     9. DemandDeposits real [105.6, 533.0]
     10. FederalFunds real [225.8, 412.1]
     11. MoneyStock real [2.86, 20.06]
     12. CheckableDeposits real [381.1, 1154.1]
     13. LoansLeases real [269.9, 803.4]
     14. SavingsDeposits real [868.1, 3550.3]
     15. TradeCurrencies real [175.6, 1758.1]
     16. 1MonthCDRate real [3.02, 20.76]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42367) of an [OpenML dataset](https://www.openml.org/d/42367). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42367/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42367/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42367/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

